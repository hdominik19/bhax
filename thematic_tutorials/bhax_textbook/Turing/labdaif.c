#include <stdio.h>
#include <curses.h>
#include <unistd.h>

int main ( void )
{
    WINDOW *ablak;
    ablak = initscr ();

    int x = 0;
    int y = 0;

    int xhal = 1;
    int yhal = 1;

    int maxx;
    int maxy;
    getmaxyx ( ablak, maxy , maxx );

    while (1) {
	mvprintw ( y, x, " " );
        x= x + xhal;
	y= y + yhal;
        
	if ( x<maxx-xhal || x>xhal) {
           xhal*=-1;
	}
	
        if ( y<maxy-yhal || y>yhal) {
           yhal*=-1;
	}
       
        mvprintw ( y, x, "O" );
	refresh ();
        usleep ( 16666 );

    }

    return 0;
}
